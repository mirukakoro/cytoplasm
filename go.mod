module gitlab.com/colourdelete/cytoplasm

go 1.15

require (
	github.com/alecthomas/participle v0.6.0
	github.com/gookit/color v1.3.1
	github.com/traefik/yaegi v0.9.2
	github.com/wayneashleyberry/terminal-dimensions v1.0.0
)
