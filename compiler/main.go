package compiler

import (
	"fmt"

	"gitlab.com/colourdelete/cytoplasm/parser"
)

// Compile is a function that converts AST to GO source code and an error.
func Compile(ast *parser.AST) (string, error) {
	if ast == nil {
		return "", fmt.Errorf("ast is %s", fmt.Sprint(ast))
	}
	var src = golangTemplate
	for _, call := range ast.Calls {
		tmp, err := CompileCall(call, 0)
		if err != nil {
			return "", err // TODO: Skip this line in compilation if err != nil with an option like ignoreCompileError
		}
		src += tmp + "\n"
	}
	src += "}\n"
	return src, nil
}

// CompileExpr is a function to compile Exprs.
func CompileExpr(expr *parser.Expr, _ int) (string, error) {
	var src = ""
	tmp, err := CompileCall(&expr.Call, 0)
	if err != nil {
		return "", err
	}
	src += tmp
	tmp, err = CompileNum(expr.Num, 0)
	if err != nil {
		return "", err
	}
	src += tmp
	return src, nil
}

// CompileCall is a function to compile Calls.
func CompileCall(call *parser.Call, _ int) (string, error) {
	if call.Ident == "" {
		return "", nil
	}
	// if len(call.Args) == 0 {
	// 	return "", nil
	// }
	if _, ok := allowedCallees[call.Ident]; !ok {
		return "", fmt.Errorf("callee %s doesn't exist", call.Ident)
	}
	var args = ""
	for _, expr := range call.Args {
		tmp, err := CompileExpr(expr, 0)
		if err != nil {
			return "", err
		}
		args += tmp + ", "
	}
	return "cyCall(\"" + call.Ident + "\", " + args + ")", nil
}

// CompileNum is a function to compile Nums.
func CompileNum(num string, _ int) (string, error) {
	return num, nil
}

// TODO: Add support for functions by returning func(vars... float64) float64 instead of float64.
// TODO: Future Feature: Make Callee from Calls with an Ident as an Arg.
// // CompileIdent is a function to compile Nums.
// func CompileIdent(ident string, _ int) (string, error) {
// 	return fmt.Sprintf(`cyVar{name: %s}`, ident), nil
// }
