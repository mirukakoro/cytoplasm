package parser

import (
	"github.com/alecthomas/participle/lexer"
)

var iniLexer = lexer.Must(lexer.Regexp(
	`(?m)` +
	`(\s+)` +
	`|(^[#;].*$)` +
	`|(?P<Ident>[a-zA-Z][a-zA-Z0-9]*)` +
	`|(?P<Num>\d+(?:\.\d+)?|\d+)` + 
	`|(?P<Comma>,)` + 
	`|(?P<OParen>\()` + 
	`|(?P<CParen>\))`,
))

// AST is a Cytoplasm AST struct which contains 0 or more Calls.
type AST struct {
	Calls []*Call `@@*`
}

// Call is a struct with an ID and args. It represents Cytoplasm function calls.
type Call struct {
	Ident string  `@Ident`
	OParen string `@OParen`
	Args []*Expr `(@@ ("," @@)*)?`
	CParen string `@CParen`
}

// Expr is a struct which represents an expression (Num, Ident or Call).
type Expr struct {
	Num string `@Num`
	Call Call `|@@`
	// TODO: Future Feature: Make Callee from Calls with an Ident as an Arg.
	// Ident string `|@Ident`
	
}
