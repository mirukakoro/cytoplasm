FROM gitpod/workspace-full

USER gitpod
# I forgot if it was golang or go sry :(
RUN sudo apt-get -q update && \
    sudo apt-get install -yq golang && \
    sudo rm -rf /var/lib/apt/lists/*
RUN sudo apt-get -q update && \
    sudo apt-get install -yq go && \
    sudo rm -rf /var/lib/apt/lists/*
