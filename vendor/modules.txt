# github.com/alecthomas/participle v0.6.0
## explicit
github.com/alecthomas/participle
github.com/alecthomas/participle/lexer
# github.com/gookit/color v1.3.1
## explicit
github.com/gookit/color
# github.com/traefik/yaegi v0.9.2
## explicit
github.com/traefik/yaegi/interp
github.com/traefik/yaegi/stdlib
# github.com/wayneashleyberry/terminal-dimensions v1.0.0
## explicit
github.com/wayneashleyberry/terminal-dimensions
