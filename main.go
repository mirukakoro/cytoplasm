package main

import (
	"flag"
	"fmt"
	"go/format"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/traefik/yaegi/interp"
	"github.com/traefik/yaegi/stdlib"
	"gitlab.com/colourdelete/cytoplasm/compiler"
	"gitlab.com/colourdelete/cytoplasm/parser"
	"gitlab.com/colourdelete/cytoplasm/ui"
	terminal "github.com/wayneashleyberry/terminal-dimensions"
)

var inSrcFlag = flag.String("in", "./in.cy", "Input file path.")
var outSrcFlag = flag.String("out", "./out-cy.go", "Output file path.")
var engineFlag = flag.String("engine", "nil", "Engine to use for compiling/interpreting.")
var fmtSrcFlag = flag.Bool("fmt", false, "Format output when usign engine nil.")
var maxProgress = 7

func main() {
	start := time.Now()
	ui.PrintProgress(0, maxProgress, "↻ Cytoplasm Compiler v1.0", "info")
	defer func() {
        if err := recover(); err != nil {
			ui.PrintProgress(maxProgress, maxProgress, fmt.Sprintf("✗ Error (recovered panic): %s", err), "critical")
			fmt.Println()
			os.Exit(10)
        }
	}()
	ui.PrintProgress(1, maxProgress, "↻ Setting up...", "info")
	flag.Parse()
	var inSrc = *inSrcFlag
	var outSrc = *outSrcFlag
	var engine = *engineFlag
	var fmtSrc = *fmtSrcFlag
	if engine != "nil" && engine != "yaegi" {
		ui.PrintProgress(1, maxProgress, fmt.Sprintf("✗ Setting up: engine %s isn't available", engine), "critical")
		fmt.Println()
		os.Exit(10)
	}
	ui.PrintProgress(2, maxProgress, "↻ Reading from `in.cy`...", "info")
	file, err := os.Open(inSrc)
	if err != nil {
		ui.PrintProgress(2, maxProgress, fmt.Sprintf("✗ Reading from `%s`: %s", inSrc, err), "critical")
		fmt.Println()
		os.Exit(11)
	}
	ui.PrintProgress(3, maxProgress, "↻ Parsing...", "info")
	ast, err := parser.Parse(file)
	if err != nil {
		ui.PrintProgress(3, maxProgress, fmt.Sprintf("✗ Parsing: %s", err), "critical")
		fmt.Println()
		os.Exit(12)
	}
	ui.PrintProgress(4, maxProgress, "↻ Compiling...", "info")
	src, err := compiler.Compile(ast)
	if err != nil {
		ui.PrintProgress(4, maxProgress, fmt.Sprintf("✗ Compiling: %s", err), "critical")
		fmt.Println()
		os.Exit(13)
	}
	if engine == "nil" {
		if fmtSrc {
			ui.PrintProgress(5, maxProgress, "↻ Formatting compiled program...", "info")
			tmp, err := format.Source([]byte(src))
			src = string(tmp)
			if err != nil {
				ui.PrintProgress(5, maxProgress, fmt.Sprintf("✗ Formatting compiled program: %s", err), "critical")
				fmt.Println()
				os.Exit(14)
			}
		} else {
			ui.PrintProgress(5, maxProgress, "→ Not formatting compiled program...", "info")
		}
		ui.PrintProgress(6, maxProgress, fmt.Sprintf("↻ Writing to `%s`...", outSrc), "info")
		err = ioutil.WriteFile("cy-out.go", []byte(src), 0644)
		if err != nil {
			ui.PrintProgress(6, maxProgress, fmt.Sprintf("✗ Writing to `%s`: %s", outSrc, err), "critical")
			fmt.Println()
			os.Exit(14)
		}
		ui.PrintlnProgress(7, maxProgress, fmt.Sprintf("✓ Done in %v using engine %s.", time.Since(start), engine), "success")
		fmt.Println()
	} else if engine == "yaegi" {
		ui.PrintlnProgress(7, maxProgress, fmt.Sprintf("✓ Done in %v using engine %s.", time.Since(start), engine), "success")
		var width uint = 0
		width, err = terminal.Width()
		fmt.Println(strings.Repeat("-", int(width)))
		ui.PrintProgress(1, 2, "↻ Interpreting using engine yaegi...", "info")
		fmt.Println()
		interpStart := time.Now()
		i := interp.New(interp.Options{})
		i.Use(stdlib.Symbols)
		_, err := i.Eval(src)
		if err != nil {
			panic(err)
		}
		ui.PrintProgress(2, 2, fmt.Sprintf("✓ Interpreted in %v using engine yaegi.", time.Since(interpStart)), "success")
	}
}
